Repositorio del taller NEA Connect 2017

* Como clonar el repositorio con git

	1. Instalar git 
	
		https://git-scm.com/download/
		
	2. Con la consola navegar hasta el directorio donde deseen clonar el repositorio, 
	   o si tienen windows e instalaron el git como viene por defecto, navegar en el 
	   explorardor hasta la carpeta deseada, Click derecho > Git bash here
	   
	3. Ingresar en la consola (copiar y pegar haciendo click derecho):
		
		git clone https://gitlab.com/neaconnect/neaconnect17.git
	
	4. Ejecutar Unity y abrir el proyecto seleccionando la carpeta que se creo
