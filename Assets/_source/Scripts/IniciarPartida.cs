﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IniciarPartida : MonoBehaviour {

    public GameObject canvasInicioDePartida;
    public GameObject prefabJugador;

	public void InitGame()
    {
        canvasInicioDePartida.SetActive(false);
        //Resources
        Instantiate<GameObject>(prefabJugador);
    }
}
