﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DesactivarObjeto : MonoBehaviour {

    public GameObject target;

    public bool activate = false;

    public float activateDelay;

    private void Start()
    {
        if(target == null)
        {
            target = gameObject;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        Debug.Log("OnTriggerEnter " + other.gameObject.name);
        //other.tag
       if(other.CompareTag("Player"))
        {
            Invoke("Activate", activateDelay);
        }
    }

    private void Activate()
    {
        target.SetActive(activate);
    }
}
