﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public float speed;
    public float jumpPower;

    public float maxDistance = 1;

    public Text recolectableText;

    public Text winText;

    public int recolectablesInScene;

    private Rigidbody rig;

    private int recolectableCount;

    private void Start()
    {
        rig = GetComponent<Rigidbody>();
    }

    void FixedUpdate () {
        //transform.Translate(Vector3.forward * Time.deltaTime);

        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        rig.AddForce(new Vector3(x,0,z) * speed);

        //Input.GetButtonDown("Jump")
        //Input.GetKeyDown(KeyCode.Space)

        if (Input.GetButtonDown("Jump"))
        {
            if(Physics.Raycast(transform.position,Vector3.down, maxDistance))
            {
                rig.AddForce(Vector3.up * jumpPower, ForceMode.Impulse);
            }
        }
	}

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Recolectable"))
        {
            recolectableCount++;
            recolectableText.text = "Objetos recolectados: " + recolectableCount;

            if (recolectableCount == recolectablesInScene)
            {
                winText.gameObject.SetActive(true);
                rig.isKinematic = true;
            }
        }
    }
}
